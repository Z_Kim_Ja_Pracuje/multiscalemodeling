﻿using Newtonsoft.Json;

namespace Multiscale {
    public class Seed {
        public Seed() {
            Id = 0;
            OnBorder = false;
        }

        public Seed(int id) {
            Id = id;
        }

        public Seed(bool IsInc) {
            if (IsInc) {
                IsInclusion = true;
                Id = -1;
            }
        }

        public Seed(Seed s) {
            Id = s.Id;
            OnBorder = s.OnBorder;
            IsInclusion = s.IsInclusion;
            IsSubstructure = s.IsSubstructure;
        }

        public static implicit operator int(Seed s) => s.Id;

        [JsonProperty("a")]
        public int Id { get; set; }

        [JsonProperty("b")]
        public bool OnBorder { get; set; }

        [JsonProperty("c")]
        public bool IsInclusion { get; set; }

        [JsonProperty("d")]
        public bool IsSubstructure { get; set; }

        public int H { get; set; }
    }
}
