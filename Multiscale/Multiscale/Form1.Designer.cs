﻿namespace Multiscale {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toTxtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toBmpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelNotification = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxXSize = new System.Windows.Forms.TextBox();
            this.textBoxYSize = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nucleonAmountField = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.inclusionAmountField = new System.Windows.Forms.NumericUpDown();
            this.inclusionRadiusField = new System.Windows.Forms.NumericUpDown();
            this.inclusionTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.structureComboBox = new System.Windows.Forms.ComboBox();
            this.button4 = new System.Windows.Forms.Button();
            this.probabilityField = new System.Windows.Forms.NumericUpDown();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.borderWidthField = new System.Windows.Forms.NumericUpDown();
            this.button5 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.PictureBoxMonteCarlo = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.StepNumberNotification = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.StateNumberField = new System.Windows.Forms.NumericUpDown();
            this.SimulationStepsField = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.NucleonAmountMCField = new System.Windows.Forms.NumericUpDown();
            this.ProbabilityMCField = new System.Windows.Forms.NumericUpDown();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.DualPhaseMCCombo = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.InitialStructureMCCombo = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.NucleonMCCombo = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.NucleonMCField = new System.Windows.Forms.NumericUpDown();
            this.label22 = new System.Windows.Forms.Label();
            this.NucleonLocationMCCombo = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.SimulationStepRecField = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.StoredEnergyMCCombo = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.EnergyMCField = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.BoundaryEnergyMCField = new System.Windows.Forms.NumericUpDown();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.InitStructureMCField1 = new System.Windows.Forms.NumericUpDown();
            this.InitStructureMCField2 = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nucleonAmountField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inclusionAmountField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.inclusionRadiusField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.probabilityField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderWidthField)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMonteCarlo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StateNumberField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SimulationStepsField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NucleonAmountMCField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProbabilityMCField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NucleonMCField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SimulationStepRecField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnergyMCField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoundaryEnergyMCField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InitStructureMCField1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InitStructureMCField2)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.labelNotification});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(917, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.clearDataToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem,
            this.exportToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem1.Text = "Microstructure";
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.fromTxtToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toTxtToolStripMenuItem,
            this.toBmpToolStripMenuItem});
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(110, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // toTxtToolStripMenuItem
            // 
            this.toTxtToolStripMenuItem.Name = "toTxtToolStripMenuItem";
            this.toTxtToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.toTxtToolStripMenuItem.Text = "To txt";
            this.toTxtToolStripMenuItem.Click += new System.EventHandler(this.toTxtToolStripMenuItem_Click);
            // 
            // toBmpToolStripMenuItem
            // 
            this.toBmpToolStripMenuItem.Name = "toBmpToolStripMenuItem";
            this.toBmpToolStripMenuItem.Size = new System.Drawing.Size(115, 22);
            this.toBmpToolStripMenuItem.Text = "To bmp";
            this.toBmpToolStripMenuItem.Click += new System.EventHandler(this.toBmpToolStripMenuItem_Click);
            // 
            // clearDataToolStripMenuItem
            // 
            this.clearDataToolStripMenuItem.Name = "clearDataToolStripMenuItem";
            this.clearDataToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.clearDataToolStripMenuItem.Text = "Clear data";
            this.clearDataToolStripMenuItem.Click += new System.EventHandler(this.clearDataToolStripMenuItem_Click);
            // 
            // labelNotification
            // 
            this.labelNotification.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.labelNotification.Name = "labelNotification";
            this.labelNotification.Size = new System.Drawing.Size(12, 20);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(917, 524);
            this.tabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(909, 498);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CA";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.button1, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBoxXSize, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxYSize, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.nucleonAmountField, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.inclusionAmountField, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.inclusionRadiusField, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.inclusionTypeComboBox, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.button2, 4, 5);
            this.tableLayoutPanel1.Controls.Add(this.button3, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.structureComboBox, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.button4, 4, 9);
            this.tableLayoutPanel1.Controls.Add(this.probabilityField, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.dataGridView1, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label9, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.borderWidthField, 4, 12);
            this.tableLayoutPanel1.Controls.Add(this.button5, 4, 13);
            this.tableLayoutPanel1.Controls.Add(this.label10, 3, 11);
            this.tableLayoutPanel1.Controls.Add(this.comboBox1, 4, 11);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(903, 492);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox1, 14);
            this.pictureBox1.Size = new System.Drawing.Size(400, 404);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(528, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 40);
            this.label1.TabIndex = 1;
            this.label1.Text = "xSize";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(653, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 40);
            this.label2.TabIndex = 2;
            this.label2.Text = "ySize";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(778, 3);
            this.button1.Name = "button1";
            this.tableLayoutPanel1.SetRowSpan(this.button1, 2);
            this.button1.Size = new System.Drawing.Size(122, 74);
            this.button1.TabIndex = 3;
            this.button1.Text = "Simulation";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxXSize
            // 
            this.textBoxXSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxXSize.Location = new System.Drawing.Point(528, 43);
            this.textBoxXSize.Name = "textBoxXSize";
            this.textBoxXSize.Size = new System.Drawing.Size(119, 20);
            this.textBoxXSize.TabIndex = 4;
            this.textBoxXSize.Text = "400";
            // 
            // textBoxYSize
            // 
            this.textBoxYSize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxYSize.Location = new System.Drawing.Point(653, 43);
            this.textBoxYSize.Name = "textBoxYSize";
            this.textBoxYSize.Size = new System.Drawing.Size(119, 20);
            this.textBoxYSize.TabIndex = 5;
            this.textBoxYSize.Text = "400";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(528, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nucleon amount";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nucleonAmountField
            // 
            this.nucleonAmountField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nucleonAmountField.Location = new System.Drawing.Point(653, 83);
            this.nucleonAmountField.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nucleonAmountField.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nucleonAmountField.Name = "nucleonAmountField";
            this.nucleonAmountField.Size = new System.Drawing.Size(119, 20);
            this.nucleonAmountField.TabIndex = 7;
            this.nucleonAmountField.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(528, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(119, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "Inclusion amount";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(528, 128);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "Inclusion radius";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // inclusionAmountField
            // 
            this.inclusionAmountField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inclusionAmountField.Location = new System.Drawing.Point(653, 107);
            this.inclusionAmountField.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.inclusionAmountField.Name = "inclusionAmountField";
            this.inclusionAmountField.Size = new System.Drawing.Size(119, 20);
            this.inclusionAmountField.TabIndex = 10;
            // 
            // inclusionRadiusField
            // 
            this.inclusionRadiusField.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inclusionRadiusField.Location = new System.Drawing.Point(653, 131);
            this.inclusionRadiusField.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.inclusionRadiusField.Name = "inclusionRadiusField";
            this.inclusionRadiusField.Size = new System.Drawing.Size(119, 20);
            this.inclusionRadiusField.TabIndex = 11;
            // 
            // inclusionTypeComboBox
            // 
            this.inclusionTypeComboBox.FormattingEnabled = true;
            this.inclusionTypeComboBox.Items.AddRange(new object[] {
            "Circle",
            "Square"});
            this.inclusionTypeComboBox.Location = new System.Drawing.Point(653, 155);
            this.inclusionTypeComboBox.Name = "inclusionTypeComboBox";
            this.inclusionTypeComboBox.Size = new System.Drawing.Size(117, 21);
            this.inclusionTypeComboBox.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(528, 152);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 24);
            this.label6.TabIndex = 13;
            this.label6.Text = "Inclusion type";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.Location = new System.Drawing.Point(778, 155);
            this.button2.Name = "button2";
            this.tableLayoutPanel1.SetRowSpan(this.button2, 2);
            this.button2.Size = new System.Drawing.Size(122, 26);
            this.button2.TabIndex = 14;
            this.button2.Text = "Insert inclusions";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button3.Location = new System.Drawing.Point(778, 187);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(122, 38);
            this.button3.TabIndex = 15;
            this.button3.Text = "Clear inclusion settings";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(528, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 24);
            this.label7.TabIndex = 16;
            this.label7.Text = "Probability (in %)";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(528, 252);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 24);
            this.label8.TabIndex = 18;
            this.label8.Text = "Structure";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // structureComboBox
            // 
            this.structureComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.structureComboBox.FormattingEnabled = true;
            this.structureComboBox.Items.AddRange(new object[] {
            "None",
            "Substructure",
            "Dual phase"});
            this.structureComboBox.Location = new System.Drawing.Point(653, 255);
            this.structureComboBox.Name = "structureComboBox";
            this.structureComboBox.Size = new System.Drawing.Size(119, 21);
            this.structureComboBox.TabIndex = 19;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(778, 255);
            this.button4.Name = "button4";
            this.tableLayoutPanel1.SetRowSpan(this.button4, 2);
            this.button4.Size = new System.Drawing.Size(117, 23);
            this.button4.TabIndex = 21;
            this.button4.Text = "Dual phase";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // probabilityField
            // 
            this.probabilityField.Location = new System.Drawing.Point(653, 231);
            this.probabilityField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.probabilityField.Name = "probabilityField";
            this.probabilityField.Size = new System.Drawing.Size(117, 20);
            this.probabilityField.TabIndex = 22;
            this.probabilityField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.Size});
            this.tableLayoutPanel1.SetColumnSpan(this.dataGridView1, 2);
            this.dataGridView1.Location = new System.Drawing.Point(415, 303);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(15, 3, 3, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.tableLayoutPanel1.SetRowSpan(this.dataGridView1, 4);
            this.dataGridView1.Size = new System.Drawing.Size(228, 164);
            this.dataGridView1.TabIndex = 20;
            this.dataGridView1.Visible = false;
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 70;
            // 
            // Size
            // 
            this.Size.HeaderText = "Size - %";
            this.Size.Name = "Size";
            this.Size.ReadOnly = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(653, 324);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(119, 24);
            this.label9.TabIndex = 24;
            this.label9.Text = "Boundaries width";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // borderWidthField
            // 
            this.borderWidthField.Location = new System.Drawing.Point(778, 327);
            this.borderWidthField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.borderWidthField.Name = "borderWidthField";
            this.borderWidthField.Size = new System.Drawing.Size(117, 20);
            this.borderWidthField.TabIndex = 25;
            this.borderWidthField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button5
            // 
            this.button5.Dock = System.Windows.Forms.DockStyle.Top;
            this.button5.Location = new System.Drawing.Point(778, 351);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(122, 23);
            this.button5.TabIndex = 26;
            this.button5.Text = "Remove grains";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(653, 300);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 24);
            this.label10.TabIndex = 27;
            this.label10.Text = "Color boundaries";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "All grains",
            "Random grains"});
            this.comboBox1.Location = new System.Drawing.Point(778, 303);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(117, 21);
            this.comboBox1.TabIndex = 28;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(909, 498);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Monte Carlo";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.PictureBoxMonteCarlo, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label11, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label12, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.StepNumberNotification, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.button6, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.StateNumberField, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.SimulationStepsField, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label13, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label14, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.NucleonAmountMCField, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.ProbabilityMCField, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.button7, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.button8, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.label15, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.DualPhaseMCCombo, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.label16, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.button9, 4, 7);
            this.tableLayoutPanel2.Controls.Add(this.InitialStructureMCCombo, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.label20, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.NucleonMCCombo, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.label21, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.NucleonMCField, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.label22, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.NucleonLocationMCCombo, 2, 12);
            this.tableLayoutPanel2.Controls.Add(this.label23, 3, 7);
            this.tableLayoutPanel2.Controls.Add(this.SimulationStepRecField, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.label17, 3, 9);
            this.tableLayoutPanel2.Controls.Add(this.StoredEnergyMCCombo, 3, 10);
            this.tableLayoutPanel2.Controls.Add(this.label18, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.EnergyMCField, 3, 12);
            this.tableLayoutPanel2.Controls.Add(this.label19, 3, 13);
            this.tableLayoutPanel2.Controls.Add(this.BoundaryEnergyMCField, 3, 14);
            this.tableLayoutPanel2.Controls.Add(this.label24, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.label25, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.InitStructureMCField1, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.InitStructureMCField2, 1, 12);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 17;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(903, 492);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // PictureBoxMonteCarlo
            // 
            this.PictureBoxMonteCarlo.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxMonteCarlo.Margin = new System.Windows.Forms.Padding(0);
            this.PictureBoxMonteCarlo.Name = "PictureBoxMonteCarlo";
            this.tableLayoutPanel2.SetRowSpan(this.PictureBoxMonteCarlo, 16);
            this.PictureBoxMonteCarlo.Size = new System.Drawing.Size(400, 400);
            this.PictureBoxMonteCarlo.TabIndex = 0;
            this.PictureBoxMonteCarlo.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(403, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 24);
            this.label11.TabIndex = 1;
            this.label11.Text = "State number";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(528, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 24);
            this.label12.TabIndex = 2;
            this.label12.Text = "Simulation steps";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StepNumberNotification
            // 
            this.StepNumberNotification.AutoSize = true;
            this.StepNumberNotification.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StepNumberNotification.Location = new System.Drawing.Point(653, 0);
            this.StepNumberNotification.Name = "StepNumberNotification";
            this.StepNumberNotification.Size = new System.Drawing.Size(119, 24);
            this.StepNumberNotification.TabIndex = 3;
            this.StepNumberNotification.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(778, 3);
            this.button6.Name = "button6";
            this.tableLayoutPanel2.SetRowSpan(this.button6, 2);
            this.button6.Size = new System.Drawing.Size(122, 23);
            this.button6.TabIndex = 4;
            this.button6.Text = "Monte Carlo";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // StateNumberField
            // 
            this.StateNumberField.Location = new System.Drawing.Point(403, 27);
            this.StateNumberField.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.StateNumberField.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.StateNumberField.Name = "StateNumberField";
            this.StateNumberField.Size = new System.Drawing.Size(119, 20);
            this.StateNumberField.TabIndex = 5;
            this.StateNumberField.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // SimulationStepsField
            // 
            this.SimulationStepsField.Location = new System.Drawing.Point(528, 27);
            this.SimulationStepsField.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.SimulationStepsField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SimulationStepsField.Name = "SimulationStepsField";
            this.SimulationStepsField.Size = new System.Drawing.Size(119, 20);
            this.SimulationStepsField.TabIndex = 6;
            this.SimulationStepsField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(403, 48);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(119, 24);
            this.label13.TabIndex = 8;
            this.label13.Text = "Nucleon amount";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(528, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(119, 24);
            this.label14.TabIndex = 9;
            this.label14.Text = "Probability";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NucleonAmountMCField
            // 
            this.NucleonAmountMCField.Location = new System.Drawing.Point(403, 75);
            this.NucleonAmountMCField.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NucleonAmountMCField.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NucleonAmountMCField.Name = "NucleonAmountMCField";
            this.NucleonAmountMCField.Size = new System.Drawing.Size(119, 20);
            this.NucleonAmountMCField.TabIndex = 10;
            this.NucleonAmountMCField.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // ProbabilityMCField
            // 
            this.ProbabilityMCField.Location = new System.Drawing.Point(528, 75);
            this.ProbabilityMCField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ProbabilityMCField.Name = "ProbabilityMCField";
            this.ProbabilityMCField.Size = new System.Drawing.Size(119, 20);
            this.ProbabilityMCField.TabIndex = 11;
            this.ProbabilityMCField.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(778, 51);
            this.button7.Name = "button7";
            this.tableLayoutPanel2.SetRowSpan(this.button7, 2);
            this.button7.Size = new System.Drawing.Size(122, 23);
            this.button7.TabIndex = 7;
            this.button7.Text = "CA";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(778, 99);
            this.button8.Name = "button8";
            this.tableLayoutPanel2.SetRowSpan(this.button8, 2);
            this.button8.Size = new System.Drawing.Size(122, 23);
            this.button8.TabIndex = 12;
            this.button8.Text = "Dual phase";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(653, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(119, 24);
            this.label15.TabIndex = 13;
            this.label15.Text = "Simulation type";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DualPhaseMCCombo
            // 
            this.DualPhaseMCCombo.FormattingEnabled = true;
            this.DualPhaseMCCombo.Items.AddRange(new object[] {
            "Monte Carlo",
            "CA"});
            this.DualPhaseMCCombo.Location = new System.Drawing.Point(653, 123);
            this.DualPhaseMCCombo.Name = "DualPhaseMCCombo";
            this.DualPhaseMCCombo.Size = new System.Drawing.Size(119, 21);
            this.DualPhaseMCCombo.TabIndex = 14;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(403, 168);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(119, 24);
            this.label16.TabIndex = 16;
            this.label16.Text = "Initial structure";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(778, 171);
            this.button9.Name = "button9";
            this.tableLayoutPanel2.SetRowSpan(this.button9, 2);
            this.button9.Size = new System.Drawing.Size(119, 23);
            this.button9.TabIndex = 15;
            this.button9.Text = "Recrystalization";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // InitialStructureMCCombo
            // 
            this.InitialStructureMCCombo.FormattingEnabled = true;
            this.InitialStructureMCCombo.Items.AddRange(new object[] {
            "CA",
            "Monte Carlo"});
            this.InitialStructureMCCombo.Location = new System.Drawing.Point(403, 195);
            this.InitialStructureMCCombo.Name = "InitialStructureMCCombo";
            this.InitialStructureMCCombo.Size = new System.Drawing.Size(119, 21);
            this.InitialStructureMCCombo.TabIndex = 17;
            this.InitialStructureMCCombo.SelectedIndexChanged += new System.EventHandler(this.InitialStructureMCCombo_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(528, 168);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(119, 24);
            this.label20.TabIndex = 22;
            this.label20.Text = "Nucleon distribution";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NucleonMCCombo
            // 
            this.NucleonMCCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NucleonMCCombo.FormattingEnabled = true;
            this.NucleonMCCombo.Items.AddRange(new object[] {
            "Constant",
            "Increasing",
            "At the beginning"});
            this.NucleonMCCombo.Location = new System.Drawing.Point(528, 195);
            this.NucleonMCCombo.Name = "NucleonMCCombo";
            this.NucleonMCCombo.Size = new System.Drawing.Size(119, 21);
            this.NucleonMCCombo.TabIndex = 25;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Location = new System.Drawing.Point(528, 216);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(119, 24);
            this.label21.TabIndex = 26;
            this.label21.Text = "Nucleon amount";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NucleonMCField
            // 
            this.NucleonMCField.Location = new System.Drawing.Point(528, 243);
            this.NucleonMCField.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NucleonMCField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NucleonMCField.Name = "NucleonMCField";
            this.NucleonMCField.Size = new System.Drawing.Size(119, 20);
            this.NucleonMCField.TabIndex = 27;
            this.NucleonMCField.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Location = new System.Drawing.Point(528, 264);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(119, 24);
            this.label22.TabIndex = 28;
            this.label22.Text = "Nucleon location";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // NucleonLocationMCCombo
            // 
            this.NucleonLocationMCCombo.FormattingEnabled = true;
            this.NucleonLocationMCCombo.Items.AddRange(new object[] {
            "GB",
            "Anywhere"});
            this.NucleonLocationMCCombo.Location = new System.Drawing.Point(528, 291);
            this.NucleonLocationMCCombo.Name = "NucleonLocationMCCombo";
            this.NucleonLocationMCCombo.Size = new System.Drawing.Size(119, 21);
            this.NucleonLocationMCCombo.TabIndex = 29;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Location = new System.Drawing.Point(653, 168);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(119, 24);
            this.label23.TabIndex = 30;
            this.label23.Text = "Simulation steps";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SimulationStepRecField
            // 
            this.SimulationStepRecField.Location = new System.Drawing.Point(653, 195);
            this.SimulationStepRecField.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.SimulationStepRecField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SimulationStepRecField.Name = "SimulationStepRecField";
            this.SimulationStepRecField.Size = new System.Drawing.Size(119, 20);
            this.SimulationStepRecField.TabIndex = 31;
            this.SimulationStepRecField.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Location = new System.Drawing.Point(653, 216);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(119, 24);
            this.label17.TabIndex = 18;
            this.label17.Text = "Stored energy";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StoredEnergyMCCombo
            // 
            this.StoredEnergyMCCombo.FormattingEnabled = true;
            this.StoredEnergyMCCombo.Items.AddRange(new object[] {
            "Homogenous",
            "Heterogenous"});
            this.StoredEnergyMCCombo.Location = new System.Drawing.Point(653, 243);
            this.StoredEnergyMCCombo.Name = "StoredEnergyMCCombo";
            this.StoredEnergyMCCombo.Size = new System.Drawing.Size(119, 21);
            this.StoredEnergyMCCombo.TabIndex = 21;
            this.StoredEnergyMCCombo.SelectedIndexChanged += new System.EventHandler(this.StoredEnergyMCCombo_SelectedIndexChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(653, 264);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(119, 24);
            this.label18.TabIndex = 19;
            this.label18.Text = "Energy";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EnergyMCField
            // 
            this.EnergyMCField.Location = new System.Drawing.Point(653, 291);
            this.EnergyMCField.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.EnergyMCField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.EnergyMCField.Name = "EnergyMCField";
            this.EnergyMCField.Size = new System.Drawing.Size(119, 20);
            this.EnergyMCField.TabIndex = 23;
            this.EnergyMCField.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Enabled = false;
            this.label19.Location = new System.Drawing.Point(653, 312);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(119, 24);
            this.label19.TabIndex = 20;
            this.label19.Text = "Boundary energy";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.Visible = false;
            // 
            // BoundaryEnergyMCField
            // 
            this.BoundaryEnergyMCField.Location = new System.Drawing.Point(653, 339);
            this.BoundaryEnergyMCField.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.BoundaryEnergyMCField.Name = "BoundaryEnergyMCField";
            this.BoundaryEnergyMCField.Size = new System.Drawing.Size(119, 20);
            this.BoundaryEnergyMCField.TabIndex = 24;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Location = new System.Drawing.Point(403, 216);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(119, 24);
            this.label24.TabIndex = 32;
            this.label24.Text = "label24";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Location = new System.Drawing.Point(403, 264);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(119, 24);
            this.label25.TabIndex = 33;
            this.label25.Text = "label25";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.Visible = false;
            // 
            // InitStructureMCField1
            // 
            this.InitStructureMCField1.Location = new System.Drawing.Point(403, 243);
            this.InitStructureMCField1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.InitStructureMCField1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.InitStructureMCField1.Name = "InitStructureMCField1";
            this.InitStructureMCField1.Size = new System.Drawing.Size(119, 20);
            this.InitStructureMCField1.TabIndex = 34;
            this.InitStructureMCField1.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.InitStructureMCField1.Visible = false;
            // 
            // InitStructureMCField2
            // 
            this.InitStructureMCField2.Location = new System.Drawing.Point(403, 291);
            this.InitStructureMCField2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.InitStructureMCField2.Name = "InitStructureMCField2";
            this.InitStructureMCField2.Size = new System.Drawing.Size(119, 20);
            this.InitStructureMCField2.TabIndex = 35;
            this.InitStructureMCField2.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.InitStructureMCField2.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(917, 548);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nucleonAmountField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inclusionAmountField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.inclusionRadiusField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.probabilityField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.borderWidthField)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxMonteCarlo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StateNumberField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SimulationStepsField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NucleonAmountMCField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProbabilityMCField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NucleonMCField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SimulationStepRecField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnergyMCField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BoundaryEnergyMCField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InitStructureMCField1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InitStructureMCField2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toTxtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toBmpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem labelNotification;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxXSize;
        private System.Windows.Forms.TextBox textBoxYSize;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nucleonAmountField;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown inclusionAmountField;
        private System.Windows.Forms.NumericUpDown inclusionRadiusField;
        private System.Windows.Forms.ComboBox inclusionTypeComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox structureComboBox;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.NumericUpDown probabilityField;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn Size;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown borderWidthField;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.PictureBox PictureBoxMonteCarlo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label StepNumberNotification;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.NumericUpDown StateNumberField;
        private System.Windows.Forms.NumericUpDown SimulationStepsField;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown NucleonAmountMCField;
        private System.Windows.Forms.NumericUpDown ProbabilityMCField;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox DualPhaseMCCombo;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox InitialStructureMCCombo;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox StoredEnergyMCCombo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.NumericUpDown EnergyMCField;
        private System.Windows.Forms.NumericUpDown BoundaryEnergyMCField;
        private System.Windows.Forms.ComboBox NucleonMCCombo;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown NucleonMCField;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox NucleonLocationMCCombo;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.NumericUpDown SimulationStepRecField;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown InitStructureMCField1;
        private System.Windows.Forms.NumericUpDown InitStructureMCField2;
    }
}

