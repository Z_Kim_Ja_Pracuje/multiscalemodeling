﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using System.Linq;

namespace Multiscale {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            button4.Enabled = false;
        }

        private delegate Task awaitable(SaveFileDialog dialog);

        private int xSize;
        private int ySize;
        private int NucAmount { get; set; }
        private int InclusionAmount { get; set; }
        private int InclusionRadius { get; set; }
        private string InclusionType { get; set; }
        private string Structure { get; set; }
        private string GrainBoundaries { get; set; }
        private Seed[,] Tab { get; set; }
        private List<Color> colors = new List<Color>();
        private int hasId = 0;
        private ConcurrentBag<Point> newPointList = new ConcurrentBag<Point>();
        private bool firstRun = true;
        private List<Point> inclusionsCenter = new List<Point>();
        private int probability;
        private Random Rand { get; set; }

        private readonly int[,] fMoore = new int[,] { { -1, -1 }, { -1, 1 }, { 1, 1 }, { 1, -1 } };
        private readonly int[,] vonN = new int[,] { { 1, 0 }, { -1, 0 }, { 0, 1 }, { 0, -1 } };


        private void button1_Click(object sender, EventArgs e) {
            StartSimulation();
        }

        private void StartSimulation() {
            // prepare before
            ClearData();
            firstRun = false;
            if (!int.TryParse(textBoxXSize.Text, out xSize))
                xSize = 400;
            if (!int.TryParse(textBoxYSize.Text, out ySize))
                ySize = 400;

            //int.TryParse(probabilityTextBox.Text, out probability);
            probability = (int)probabilityField.Value;

            Tab = new Seed[xSize, ySize];
            NucAmount = (int)nucleonAmountField.Value;

            if (NucAmount <= 0) return;
            Rand = new Random();
            colors = new List<Color>();

            Structure = !string.IsNullOrEmpty(structureComboBox.Text) ? structureComboBox.Text : "";

            InsertInclusions(Tab);

            InsertNucleons();

            DrawOnPictureBox();
            hasId += NucAmount;

            Simulate();

            GrainBoundaries = comboBox1.Text;
            // DrawOnPictureBox();
            if (!string.IsNullOrEmpty(GrainBoundaries) && GrainBoundaries != "None") {
                ColorBorders();
            }
            else {
                button4.Enabled = true;
            }
        }

        private void ColorBorders(bool clearGrains = false) {
            if (!clearGrains) {
                int borderWidth = (int)borderWidthField.Value;
                List<int> ids = new List<int>();

                if (GrainBoundaries == "Random grains") {
                    ClearBorder();
                    Rand = new Random();
                    int count = Rand.Next(NucAmount);
                    for (int i = 0; i < count; i++) {
                        int j = 0;
                        do {
                            j = Rand.Next(1, NucAmount + 1);
                        }
                        while (ids.Contains(j));
                        ids.Add(j);
                    }
                    for (int i = 0; i < borderWidth; i++) {
                        CheckOnBorder(true, ids);
                    }
                }
                else {
                    for (int i = 1; i < borderWidth; i++) {
                        CheckOnBorder(true, null);
                    }
                }


            }
            int counter = 0;
            Bitmap b = (Bitmap)pictureBox1.Image;
            for (int i = 0; i < xSize; i++) {
                for (int j = 0; j < ySize; j++) {
                    if (Tab[i, j].OnBorder) {
                        b.SetPixel(i, j, Color.Black);
                        counter++;
                    }
                    else if (clearGrains)
                        b.SetPixel(i, j, Color.Transparent);
                }
            }
            dataGridView1.Rows.Clear();
            dataGridView1.Rows.Add("Boundaries", "" + counter + ", " + Math.Round(100 * counter / (double)(xSize * ySize), 2) + "%");
            dataGridView1.Visible = true;
            pictureBox1.Refresh();
        }

        private void ClearBorder() {
            for (int i = 0; i < xSize; i++) {
                for (int j = 0; j < ySize; j++) {
                    if (Tab[i, j] != null)
                        Tab[i, j].OnBorder = false;
                }
            }
        }



        private void DualPhaseSimulation() {
            Rand = new Random();
            newPointList = new ConcurrentBag<Point>();
            Structure = !string.IsNullOrEmpty(structureComboBox.Text) ? structureComboBox.Text : "";
            probability = (int)probabilityField.Value;
            int count = 0;
            if (!string.IsNullOrEmpty(Structure) && Structure != "None") {
                hasId = 0;
                count = PrepareForStructure();
                NucAmount = (int)nucleonAmountField.Value;

                InsertNucleons(count + 1);
                hasId += NucAmount;
                DrawOnPictureBox();
                Simulate();
            }
            DrawOnPictureBox();

            CalculatePercentage(count);
        }

        private void InsertNucleons(int startNucleon = 1) {
            for (int i = 1; i <= NucAmount; i++) {
                int a = 0;
                int b = 0;

                do {
                    a = Rand.Next(xSize);
                    b = Rand.Next(ySize);
                }
                while (Tab[a, b] != null);
                Tab[a, b] = new Seed(startNucleon++);
                colors.Add(Color.FromArgb(Rand.Next(256), Rand.Next(256), Rand.Next(256)));
                newPointList.Add(new Point(a, b));
            }
        }

        private void Simulate() {
            Seed[,] tabTemp = (Seed[,])Tab.Clone();
            int preHasId = 0;
            while (hasId < xSize * ySize) {
                preHasId = hasId;
                InsertIntoTabTemp(tabTemp);
                newPointList = new ConcurrentBag<Point>();
                Parallel.For(0, xSize, index => {
                    IDictionary<int, int> dictionary = new Dictionary<int, int>();
                    for (int j = 0; j < ySize; j++) {
                        if (Tab[index, j] == null) {
                            CheckNeighboursMoore(index, j, tabTemp, dictionary);
                            //CheckFurtherMoore(index, j, tabTemp, dictionary);
                        }

                    }
                });

                DrawOnPictureBox();
                //if (preHasId == hasId)
                //    break;
            }
            CheckOnBorder();
        }

        private bool CheckNeighboursMoore(int i, int j, Seed[,] tabTemp, IDictionary<int, int> dictionary, bool prob = false) {
            dictionary.Clear();
            bool isSth = false;
            int count = 0;
            for (int a = i - 1; a < i + 2; a++) {
                for (int b = j - 1; b < j + 2; b++) {
                    if (a == b) continue;
                    if (a < 0 || a >= xSize || b < 0 || b >= ySize) continue;
                    if (tabTemp[a, b] == null || tabTemp[a, b].IsInclusion || tabTemp[a, b].IsSubstructure) continue;
                    isSth = true;
                    count++;
                    //if exists
                    if (dictionary.ContainsKey(tabTemp[a, b].Id))
                        dictionary[tabTemp[a, b].Id]++;
                    else
                        dictionary.Add(tabTemp[a, b].Id, 1);
                }
            }
            if (!isSth) return false;
            int maxId = FindMaxId(dictionary);
            if (count < 3)
                prob = true;
            // 5 lub więcej sąsiadów o tym samym stanie - reguła nr 1
            if (dictionary[maxId] >= 5) {
                AddPoint(i, j, maxId);
                return true;
            }
            // reguły nr 2 i 3 -wywołanie nr 3 z poziomu metody CheckVonNeumann()
            // jeżeli się udało - koniec
            if (dictionary[maxId] < 5 && !prob) {
                isSth = CheckVonNeumann(i, j, tabTemp, dictionary);
                if (isSth) return true;
            }
            // reguła nr 4 - jeżeli nie udało się wykonać 2 i 3 lub
            // z poziomu reguły nr 1 zostało zweryfikowane, że 2 i3 się nie wykonają
            if (!isSth || prob) {
                if (Rand.Next(100) < probability) {
                    AddPoint(i, j, maxId);
                }
            }

            return true;
        }

        private int FindMaxId(IDictionary<int, int> dictionary) {
            int maxVal = 0;
            int maxKey = 0;
            foreach (var v in dictionary) {
                if (v.Value > maxVal) {
                    maxVal = v.Value;
                    maxKey = v.Key;
                }
            }
            return maxKey;
        }

        private void AddPoint(int i, int j, int maxId) {
            Tab[i, j] = new Seed(maxId);
            Interlocked.Add(ref hasId, 1);
            newPointList.Add(new Point(i, j));
        }

        private bool CheckVonNeumann(int i, int j, Seed[,] tabTemp, IDictionary<int, int> dictionary) {
            //IDictionary<int, int> dictionary = new Dictionary<int, int>();
            dictionary.Clear();
            for (int a = 0; a < 4; a++) {
                int i1 = i + vonN[a, 0];
                int j1 = j + vonN[a, 1];
                if (i1 >= xSize || i1 < 0 || j1 >= ySize || j1 < 0) continue;
                if (tabTemp[i1, j1] == null || tabTemp[i1, j1].IsInclusion || tabTemp[i1, j1].IsSubstructure) continue;
                if (dictionary.ContainsKey(tabTemp[i1, j1]))
                    dictionary[tabTemp[i1, j1]]++;
                else
                    dictionary.Add(tabTemp[i1, j1], 1);
            }
            int maxId = FindMaxId(dictionary);
            if (maxId == 0 || dictionary[maxId] < 3) {
                return CheckFurtherMoore(i, j, tabTemp, dictionary);
            }

            AddPoint(i, j, maxId);
            return true;
        }

        private bool CheckFurtherMoore(int i, int j, Seed[,] tabTemp, IDictionary<int, int> dictionary) {
            //IDictionary<int, int> dictionary = new Dictionary<int, int>();
            dictionary.Clear();
            for (int a = 0; a < 4; a++) {
                int i1 = i + fMoore[a, 0];
                int j1 = j + fMoore[a, 1];
                if (i1 >= xSize || i1 < 0 || j1 >= ySize || j1 < 0) continue;
                if (tabTemp[i1, j1] == null || tabTemp[i1, j1].IsInclusion || tabTemp[i1, j1].IsSubstructure) continue;
                if (dictionary.ContainsKey(tabTemp[i1, j1]))
                    dictionary[tabTemp[i1, j1]]++;
                else
                    dictionary.Add(tabTemp[i1, j1], 1);
            }

            int maxId = FindMaxId(dictionary);
            if (maxId == 0 || dictionary[maxId] < 3)
                return false;

            AddPoint(i, j, maxId);
            return true;
        }

        private void CheckOnBorder(bool checkFurther = false, List<int> ids = null) {
            // głęboka kopia !!!
            Seed[,] tabTemp = new Seed[xSize, ySize];
            for (int i = 0; i < xSize; i++) {
                for (int j = 0; j < ySize; j++) {
                    tabTemp[i, j] = new Seed(Tab[i, j]);
                }
            }

            for (int i = 0; i < xSize; i++)
                for (int j = 0; j < ySize; j++) {
                    if (Tab[i, j].OnBorder) continue;
                    if (ids != null && !ids.Contains(Tab[i, j])) continue;
                    for (int a = i - 1; a <= i + 1; a++)
                        for (int b = j - 1; b <= j + 1; b++) {
                            if (a == i && b == j) continue;
                            if (a >= xSize || a < 0 || b >= ySize || b < 0) continue;
                            if (Tab[a, b] == 0 || Tab[a, b].IsInclusion) continue;

                            if (Tab[a, b].Id != Tab[i, j] || (checkFurther && tabTemp[a, b].OnBorder)) {
                                if (ids == null) Tab[a, b].OnBorder = true;
                                Tab[i, j].OnBorder = true;

                                // to ma za zadanie tylko ubicie wewnętrznych pętli
                                a = i + 2;
                                b = j + 2;
                            }
                        }
                }
        }

        private int PrepareForStructure() {
            int count = 0;
            if (NucAmount <= 4) count = 1;
            else if (NucAmount <= 10) count = 2;
            else if (NucAmount > 10) count = Rand.Next(3, NucAmount / 3);

            List<int> strucIds = new List<int>();
            for (int i = 0; i < count; i++) {
                int j = 0;
                do {
                    j = Rand.Next(1, NucAmount + 1);
                }
                while (strucIds.Contains(j));
                strucIds.Add(j);
            }
            strucIds.Sort();

            List<Color> tempColor = new List<Color>();
            Color first = Color.FromArgb(Rand.Next(255), Rand.Next(255), Rand.Next(255));
            int[] t = new int[strucIds.Max() + 1];
            for (int i = 0; i < count; i++) {
                if (Structure == "Dual phase") {
                    if (!tempColor.Contains(first))
                        tempColor.Add(first);
                    t[strucIds[i] - 1] = 1;
                }
                else {
                    tempColor.Add(colors[strucIds[i] - 1]);
                    t[strucIds[i] - 1] = i + 1;
                }
            }

            if (Structure == "Dual phase") count = 1;

            colors.Clear();
            colors.AddRange(tempColor);

            for (int i = 0; i < 400; i++) {
                for (int j = 0; j < 400; j++) {
                    if (strucIds.Contains(Tab[i, j])) {
                        Tab[i, j].IsSubstructure = true;
                        hasId++;
                        Tab[i, j].Id = t[Tab[i, j] - 1];
                        newPointList.Add(new Point(i, j));
                        if (t[Tab[i, j] - 1] == 0)
                            Console.WriteLine(Tab[i, j].Id + "    " + t[Tab[i, j] - 1]);
                    }
                    else Tab[i, j] = null;

                }
            }

            return count;
        }

        private void DrawOnPictureBox() {
            Bitmap b = (pictureBox1.Image == null || pictureBox1.Image.Size != new Size(xSize, ySize)) ? new Bitmap(xSize, ySize) : (Bitmap)pictureBox1.Image;
            foreach (var point in newPointList) {
                if (Tab[point.X, point.Y] == -1)
                    b.SetPixel(point.X, point.Y, Color.Black);
                else
                    b.SetPixel(point.X, point.Y, colors[Tab[point.X, point.Y] - 1]);
            }
            pictureBox1.Image = b;
            pictureBox1.Refresh();
        }

        private void ClearData() {
            hasId = 0;
            newPointList = new ConcurrentBag<Point>();
            inclusionsCenter.Clear();
            if (!firstRun) {
                pictureBox1.Image = new Bitmap(xSize, ySize);
                pictureBox1.Refresh();
            }
        }

        private void InsertIntoTabTemp(Seed[,] tabTemp) {
            foreach (Point p in newPointList) {
                tabTemp[p.X, p.Y] = Tab[p.X, p.Y];
            }
        }

        private async void toTxtToolStripMenuItem_Click(object sender, EventArgs e) {
            await SaveFile("txt files|*.txt", SaveTxtFile);
        }

        private async Task SaveFile(string filter, Func<SaveFileDialog, Task> func) {
            using (SaveFileDialog sfd = new SaveFileDialog()) {
                sfd.Filter = filter;
                sfd.FilterIndex = 1;
                sfd.RestoreDirectory = true;
                sfd.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
                if (sfd.ShowDialog() == DialogResult.OK) {
                    Invoke(new MethodInvoker(() => {
                        labelNotification.Text = "Wait...";
                    }));
                    await func(sfd);
                    Invoke(new MethodInvoker(() => {
                        labelNotification.Text = "File saved.";
                    }));
                }
            }
        }

        private async Task SaveTxtFile(SaveFileDialog sfd) {
            ImportTxtHelper ith = new ImportTxtHelper {
                xSize = xSize,
                ySize = ySize,
                NucAmount = NucAmount,
                Tab = Tab,
                InclusionAmount = InclusionAmount,
                InclusionRadius = InclusionRadius,
                InclusionsCenter = inclusionsCenter.ToArray()
            };

            string json = JsonConvert.SerializeObject(ith);
            StreamWriter sw = new StreamWriter(sfd.OpenFile());
            await sw.WriteAsync(json);
            sw.Close();
            sw.Dispose();
        }

        private async void toBmpToolStripMenuItem_Click(object sender, EventArgs e) {
            await SaveFile("bmp files|*.bmp", async (sfd) => {
                await Task.Run(() => {
                    pictureBox1.Image.Save(sfd.OpenFile(), System.Drawing.Imaging.ImageFormat.Bmp);
                });
            });
        }

        private async void fromTxtToolStripMenuItem_Click(object sender, EventArgs e) {
            using (OpenFileDialog dialog = new OpenFileDialog()) {
                dialog.Filter = "txt files|*.txt|bmp files|*.bmp";
                dialog.FilterIndex = 1;
                dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;
                dialog.Multiselect = false;
                dialog.RestoreDirectory = true;
                if (dialog.ShowDialog() == DialogResult.OK) {
                    await Task.Run(() => ProcessFile(dialog.OpenFile(), dialog.SafeFileName));
                }
            }
        }

        private void ProcessFile(Stream stream, string fileName) {
            string extension = fileName.Substring(fileName.Length - 3).ToLower();

            Console.WriteLine(extension);
            if (extension == "txt") {
                ImportTxtHelper ith = JsonConvert.DeserializeObject<ImportTxtHelper>(new StreamReader(stream).ReadToEnd());
                xSize = ith.xSize;
                ySize = ith.ySize;
                NucAmount = ith.NucAmount;
                Tab = ith.Tab;
                InclusionAmount = ith.InclusionAmount;
                InclusionRadius = ith.InclusionRadius;
                inclusionsCenter = new List<Point>(ith.InclusionsCenter);

                Bitmap b = new Bitmap(xSize, ySize);
                Random r = new Random();
                for (int i = 0; i < NucAmount; i++)
                    colors.Add(Color.FromArgb(r.Next(256), r.Next(256), r.Next(256)));
                for (int i = 0; i < xSize; i++)
                    for (int j = 0; j < ySize; j++)
                        if (Tab[i, j] == -1)
                            b.SetPixel(i, j, Color.Black);
                        else
                            b.SetPixel(i, j, colors[Tab[i, j] - 1]);
                LayoutHelper(b);
            }
            else if (extension == "bmp") {
                Bitmap b = new Bitmap(stream);
                xSize = b.Width;
                ySize = b.Height;
                Tab = new Seed[xSize, ySize];
                colors = new List<Color>();
                NucAmount = 0;

                for (int i = 0; i < xSize; i++)
                    for (int j = 0; j < ySize; j++) {
                        Color color = b.GetPixel(i, j);
                        if (color == Color.Black) {
                            Tab[i, j] = new Seed(true);
                            continue;
                        }
                        if (!colors.Contains(color)) {
                            colors.Add(color);
                            NucAmount++;
                        }
                        Tab[i, j] = new Seed(colors.IndexOf(color) + 1);
                    }

                LayoutHelper(b);
            }
        }

        private void LayoutHelper(Bitmap b = null) {
            this.Invoke(new MethodInvoker(() => {
                textBoxXSize.Text = xSize.ToString();
                textBoxYSize.Text = ySize.ToString();
                nucleonAmountField.Value = NucAmount;
                inclusionAmountField.Value = InclusionAmount;
                inclusionRadiusField.Value = InclusionRadius;
                textBoxXSize.Refresh();
                textBoxYSize.Refresh();
                nucleonAmountField.Refresh();
                inclusionAmountField.Refresh();
                inclusionRadiusField.Refresh();
                if (b != null) {
                    pictureBox1.Image = b;
                    pictureBox1.Refresh();
                }
            }));
        }

        private void InsertInclusions(Seed[,] tab, bool inter = false) {
            bool isCircle = false;
            SetInclusionsData();

            if (string.IsNullOrEmpty(InclusionType) || InclusionType.Equals("Circle"))
                isCircle = true;

            Random r = new Random(DateTime.Now.Millisecond);

            // if inter sprawdzaj na granicach

            for (int k = 0; k < InclusionAmount; k++) {
                int i = 0, j = 0;
                do {
                    i = r.Next(xSize);
                    j = r.Next(ySize);
                }
                while (inter && !CheckIfOnBorder(i, j, isCircle, InclusionRadius));
                //  while(inter)
                for (int di = -InclusionRadius; di <= InclusionRadius; di++)
                    for (int dj = -InclusionRadius; dj <= InclusionRadius; dj++) {
                        int x = di + i, y = dj + j;
                        if (x >= 0 && x < xSize && y >= 0 && y < ySize) {
                            if ((isCircle && Math.Sqrt(di * di + dj * dj) <= InclusionRadius && Tab[x, y] != -1)
                                || !isCircle) {

                                Tab[x, y] = new Seed(true);
                                newPointList.Add(new Point(x, y));
                                hasId++;
                            }
                        }
                    }
                inclusionsCenter.Add(new Point(i, j));
            }

            DrawOnPictureBox();
        }

        private bool CheckIfOnBorder(int i, int j, bool isCircle, int InclusionRadius) {
            if (InclusionRadius > 1) InclusionRadius /= 2;
            for (int di = -InclusionRadius; di <= InclusionRadius; di++)
                for (int dj = -InclusionRadius; dj <= InclusionRadius; dj++) {
                    int x = di + i;
                    int y = dj + j;
                    if (x >= 0 && x < xSize && y >= 0 && y < ySize) {
                        if ((isCircle && Math.Sqrt(di * di + dj * dj) <= InclusionRadius && Tab[x, y] != -1)
                            || !isCircle) {
                            if (Tab[x, y].OnBorder) return true;
                        }
                    }
                }
            return false;
        }


        private void CalculatePercentage(int count) {
            dataGridView1.Visible = true;
            int size = xSize * ySize;
            int[] temp = new int[NucAmount + 1 + count];
            for (int i = 0; i < xSize; i++) {
                for (int j = 0; j < ySize; j++) {
                    temp[Tab[i, j]]++;
                }
            }
            if (dataGridView1.Rows.Count > 0) dataGridView1.Rows.Clear();
            for (int i = 1; i < temp.Length; i++) {
                int index = dataGridView1.Rows.Add(i, temp[i] + ", " + Math.Round(temp[i] * 100 / (double)size, 2) + "%");
                dataGridView1.Rows[index].DefaultCellStyle.BackColor = colors[i - 1];
            }
            dataGridView1.Refresh();

        }

        private void button2_Click(object sender, EventArgs e) {
            if (!firstRun)
                InsertInclusions(Tab, true);
        }

        private void button3_Click(object sender, EventArgs e) {
            ClearInclusionsSettings();
            SetInclusionsData();
        }

        private void ClearInclusionsSettings() {
            inclusionAmountField.Value = 0;
            inclusionRadiusField.Value = 0;
            inclusionTypeComboBox.Text = "";
            inclusionAmountField.Refresh();
            inclusionRadiusField.Refresh();
            inclusionTypeComboBox.Refresh();
            SetInclusionsData();
        }

        private void SetInclusionsData() {
            InclusionAmount = (int)inclusionAmountField.Value;
            InclusionRadius = (int)inclusionRadiusField.Value;
            InclusionType = inclusionTypeComboBox.Text;
        }

        private void clearDataToolStripMenuItem_Click(object sender, EventArgs e) {
            ClearAllData();
        }

        private void ClearAllData() {
            xSize = 400;
            ySize = 400;
            NucAmount = 2;
            ClearInclusionsSettings();
            Tab = new Seed[xSize, ySize];
            colors = new List<Color>();
            hasId = 0;
            newPointList = new ConcurrentBag<Point>();
            firstRun = true;
            inclusionsCenter = new List<Point>();
            LayoutHelper(new Bitmap(xSize, ySize));
        }

        private void button4_Click(object sender, EventArgs e) {
            DualPhaseSimulation();
            button4.Enabled = false;
        }

        ///
        ///helper
        ///
        private Color RandColor() {
            return Color.FromArgb(Rand.Next(255), Rand.Next(255), Rand.Next(255));
        }

        private void button5_Click(object sender, EventArgs e) {
            ColorBorders(true);
        }

        #region MonteCarlo

        private int N { get; set; }
        private int MCSteps { get; set; }
        private double BoundaryEnergy { get; set; }
        private Seed[,] TabMC { get; set; }
        private bool[,] Checked { get; set; }
        private int MCSizeY { get; set; }
        private int MCSizeX { get; set; }
        private int CheckedAmount { get; set; }
        private int[,] MCDualPhase { get; set; }
        private List<Color> MCColors { get; set; }
        private int DualPhacePixels { get; set; }
        private int MCDualPhaseProb { get; set; }

        private int NDP { get; set; }
        private bool IsDP { get; set; }

        private Seed[,] MCTab { get; set; }
        // private ;
        // private List<Point> MCPoints = new List<Point>();

        private void button6_Click(object sender, EventArgs e) {
            StartMonteCarloAsync();
        }

        private void StartMonteCarloAsync() {
            N = (int)StateNumberField.Value;
            MCSteps = (int)SimulationStepsField.Value;
            BoundaryEnergy = 1.0;
            MCSizeX = PictureBoxMonteCarlo.Width;
            MCSizeY = PictureBoxMonteCarlo.Height;

            TabMC = new Seed[MCSizeX, MCSizeY];
            Checked = new bool[MCSizeX, MCSizeY];
            MCColors = new List<Color>();
            CheckedAmount = 0;
            MonteCarloSimulation();
        }

        private void MonteCarloSimulation(Bitmap b = null, int DualPhasePixels = 0) {
            Bitmap Bitm;
            Dictionary<int, int> data;
            if (b == null) {
                Bitm = new Bitmap(MCSizeX, MCSizeY);
                PictureBoxMonteCarlo.Image = Bitm;
            }
            else {
                Bitm = b;
            }

            Random RandomGen = new Random();
            data = new Dictionary<int, int>();
            for (int i = 0; i < N; i++) {
                MCColors.Add(Color.FromArgb(RandomGen.Next(255), RandomGen.Next(255), RandomGen.Next(255)));
            }

            for (int i = 0; i < MCSizeX; i++)
                for (int j = 0; j < MCSizeY; j++) {
                    if (TabMC[i,j] != null && TabMC[i, j] == -1) {
                        // AddPoint(i, j, Bitm, Color.Black);
                        continue;
                    }
                    TabMC[i, j] = new Seed(RandomGen.Next(1, N + 1));
                    AddPoint(i, j, Bitm, MCColors);
                }
            CheckedAmount = DualPhasePixels;
            for (int i = 0; i < MCSteps; i++) {
                int x = -1;
                int y = -1;

                while (CheckedAmount < MCSizeY * MCSizeX) {
                    do {
                        x = RandomGen.Next(MCSizeX);
                        y = RandomGen.Next(MCSizeY);
                    }
                    while (Checked[x, y] || TabMC[x, y] == -1);
                    CheckedAmount++;
                    Checked[x, y] = true;
                    CheckPixel(x, y, RandomGen, data, Bitm);
                }

                CheckedAmount = DualPhasePixels;
                for (int k = 0; k < 400; k++)
                    for (int j = 0; j < 400; j++) {
                        Checked[k, j] = false;
                    }

                PictureBoxMonteCarlo.Refresh();

                //PictureBoxMonteCarlo.Invoke(new MethodInvoker(
                //    () => PictureBoxMonteCarlo.Refresh()
                //));
                //StepNumberNotification.Invoke(new MethodInvoker(() => labelNotification.Text =
                //    (i + 1 == MCSteps) ? "End of simulation." : String.Format("Iteration {0} of {1}", i, MCSteps)));

            }
            //PictureBoxMonteCarlo.Invoke(new MethodInvoker(
            //       () => PictureBoxMonteCarlo.Refresh()
            //   ));

            PictureBoxMonteCarlo.Refresh();

        }

        private void CheckPixel(int x, int y, Random rand, Dictionary<int, int> data, Bitmap Bitm) {
            data.Clear();
            List<int> neighbors = new List<int>();
            int energyAmount = CheckEnergy(TabMC, x, y, TabMC[x, y], neighbors);
            if (neighbors.Count > 0) {
                int newId = neighbors[rand.Next(0, neighbors.Count)];
                int newEnergy = CheckEnergy(TabMC, x, y, newId);
                if (newEnergy <= energyAmount) {
                    TabMC[x, y] = new Seed(newId);
                    AddPoint(x, y, Bitm, MCColors);
                }
            }

        }

        private void AddPoint(int x, int y, Bitmap bit, List<Color> col) {
            try {
                bit.SetPixel(x, y, col[TabMC[x, y] - 1]);
            }
            catch (InvalidOperationException ex) {
                this.Invoke(new MethodInvoker(
                    () => bit.SetPixel(x, y, col[TabMC[x, y] - 1]))
                );
            }
        }

        private void AddPoint(int x, int y, Bitmap bit, Color c) {
            bit.SetPixel(x, y, c);
        }

        private int CheckEnergy(Seed[,] t, int i, int j, int id, List<int> n = null, bool rec = false) {
            int energy = 0;
            for (int a = i - 1; a <= i + 1; a++) {
                for (int b = j - 1; b <= j + 1; b++) {
                    if (a < 0 || b < 0 || a >= MCSizeX || b >= MCSizeY) continue;
                    if (a == i && b == j) continue;
                    if (t[a, b] == -1) continue;
                    if (t[a, b] != id) {
                        energy++;
                        if (n != null && !n.Contains(t[a, b])) {
                            n.Add(t[a, b]);
                        }
                    }
                }
            }
            return energy;
        }

        private void button8_Click(object sender, EventArgs e) {
            DualPhaseMCSimulation();
        }


        private void DualPhaseMCSimulation() {
            int count = MCColors.Count;
            int dPseeds = count / 2;
            List<int> ids = new List<int>();
            Random r = new Random();
            Bitmap bitm = (Bitmap)PictureBoxMonteCarlo.Image;

            for (int i = 0; i < dPseeds; i++) {
                int id = -1;
                do {
                    id = r.Next(1, count + 1);
                }
                while (ids.Contains(id));
                ids.Add(id);
            }
            PrepareMCDualPhase(TabMC, ids, bitm);

            if (DualPhaseMCCombo.Text.ToLower() == "monte carlo") {
                MonteCarloSimulation(bitm, DualPhacePixels);
            }
            else {
                InsertNucleons(N, TabMC, MCColors, bitm, r, 1, true);
                DualPhacePixels += N;
                Simulate(TabMC, bitm, DualPhacePixels);
            }

            // clear data
            DualPhacePixels = 0;
        }

        private void PrepareMCDualPhase(Seed[,] t, List<int> ids, Bitmap bitm) {
            DualPhacePixels = 0;
            for (int i = 0; i < MCSizeX; i++)
                for (int j = 0; j < MCSizeY; j++) {
                    if (ids.Contains(t[i, j])) {
                        t[i, j].Id = -1;
                        DualPhacePixels++;
                        AddPoint(i, j, bitm, Color.Black);
                    }
                    else {
                        t[i, j] = null;
                        AddPoint(i, j, bitm, Color.White);
                    }
                }
            MCColors.Clear();

            if (DualPhaseMCCombo.Text.ToLower() == "monte carlo") {
                N = (int)StateNumberField.Value;
                MCSteps = (int)SimulationStepsField.Value;
                BoundaryEnergy = 1.0;
            }
            else {
                N = (int)NucleonAmountMCField.Value;
                MCDualPhaseProb = (int)ProbabilityMCField.Value;
            }
        }


        private void button7_Click(object sender, EventArgs e) {
            N = (int)NucleonAmountMCField.Value;
            MCDualPhaseProb = (int)ProbabilityMCField.Value;
            MCSizeX = PictureBoxMonteCarlo.Width;
            MCSizeY = PictureBoxMonteCarlo.Height;

            TabMC = new Seed[MCSizeX, MCSizeY];
            Checked = new bool[MCSizeX, MCSizeY];
            MCColors = new List<Color>();
            CheckedAmount = 0;
            Bitmap bit = new Bitmap(MCSizeX, MCSizeY);
            Random r = new Random();
            InsertNucleons(N, TabMC, MCColors, bit, r);
            Simulate(TabMC, bit, N);
        }

        private void InsertNucleons(int nucAmount, Seed[,] Tab, List<Color> colors, Bitmap bit, Random Rand, int startNucleon = 1, bool dp = false) {
            for (int i = 1; i <= nucAmount; i++) {
                int a = 0;
                int b = 0;

                do {
                    a = Rand.Next(MCSizeX);
                    b = Rand.Next(MCSizeY);
                }
                while (Tab[a, b] != null);
                Tab[a, b] = new Seed(i);
                colors.Add(Color.FromArgb(Rand.Next(256), Rand.Next(256), Rand.Next(256)));
                AddPoint(a, b, bit, colors[i - 1]);
            }
        }

        private void Simulate(Seed[,] tab, Bitmap bit, int hasId) {
            Seed[,] tabTemp = (Seed[,])tab.Clone();
            IDictionary<int, int> dictionary = new Dictionary<int, int>();
            Random r = new Random();
            PictureBoxMonteCarlo.Image = bit;
            int preHasId;
            int counter = 70;
            int c = 0;
            while (hasId < MCSizeX * MCSizeY) {
                preHasId = hasId;
                tabTemp = (Seed[,])tab.Clone();
                for (int i = 0; i < MCSizeX; i++) {
                    for (int j = 0; j < MCSizeY; j++) {
                        if (tab[i, j] == null) {
                            CheckNeighboursMoore(i, j, tabTemp, tab, bit, ref hasId, dictionary, MCColors, r);
                        }
                    }
                }

                PictureBoxMonteCarlo.Refresh();

                if (preHasId == hasId) {
                    c++;
                    if (c >= counter)
                        break;
                }
                else {
                    c = 0;
                }
                Console.WriteLine(hasId);
            }
        }

        private bool CheckNeighboursMoore(int i, int j, Seed[,] tabTemp, Seed[,] tab,
                Bitmap bit, ref int hasId, IDictionary<int, int> dictionary, List<Color> colors, Random r) {

            dictionary.Clear();
            bool isSth = false;
            int count = 0;
            bool prob = false;
            for (int a = i - 1; a < i + 2; a++) {
                for (int b = j - 1; b < j + 2; b++) {
                    if (a == i && j == b) continue;
                    if (a < 0 || a >= MCSizeX || b < 0 || b >= MCSizeX) continue;
                    if (tabTemp[a, b] == null) continue;
                    if (tabTemp[a, b] > 0)
                        isSth = true;
                    count++;
                    if (dictionary.ContainsKey(tabTemp[a, b]))
                        dictionary[tabTemp[a, b]]++;
                    else
                        dictionary.Add(tabTemp[a, b], 1);
                }
            }
            if (!isSth) return false;
            int maxId = FindMaxId(dictionary);
            if (maxId == -1) return false;
            if (count < 3) prob = true;

            // regula nr 1
            if (dictionary[maxId] >= 5) {
                tab[i, j] = new Seed(maxId);
                AddPoint(i, j, bit, colors[maxId - 1]);
                hasId++;
                return true;
            }

            // regula 2 i 3
            if (!prob) {
                isSth = CheckVonNeumann(i, j, tabTemp, tab, bit, dictionary, colors);
                if (isSth) {
                    hasId++;
                    return true;
                }
            }

            // regula 4
            if (!isSth || prob) {
                if (r.Next(0, 101) < MCDualPhaseProb) {
                    tab[i, j] = new Seed(maxId);
                    AddPoint(i, j, bit, colors[maxId - 1]);
                    hasId++;
                    return true;
                }
            }

            return false;
        }

        private bool CheckVonNeumann(int i, int j, Seed[,] tabTemp, Seed[,] tab,
            Bitmap bit, IDictionary<int, int> dictionary, List<Color> colors) {

            dictionary.Clear();
            for (int a = 0; a < 4; a++) {
                int i1 = i + vonN[a, 0];
                int j1 = j + vonN[a, 1];
                if (i1 >= xSize || i1 < 0 || j1 >= ySize || j1 < 0) continue;
                if (tabTemp[i1, j1] == null ) continue;
                if (dictionary.ContainsKey(tabTemp[i1, j1]))
                    dictionary[tabTemp[i1, j1]]++;
                else
                    dictionary.Add(tabTemp[i1, j1], 1);
            }
            int maxId = FindMaxId(dictionary);
            if (maxId == 0 || dictionary[maxId] < 3) {
                return CheckFurtherMoore(i, j, tabTemp, tab, bit, dictionary, colors);
            }

            tab[i, j] = new Seed(maxId);
            AddPoint(i, j, bit, colors[maxId - 1]);
            return true;
        }

        private bool CheckFurtherMoore(int i, int j, Seed[,] tabTemp, Seed[,] tab,
            Bitmap bit, IDictionary<int, int> dictionary, List<Color> colors) {

            dictionary.Clear();
            for (int a = 0; a < 4; a++) {
                int i1 = i + fMoore[a, 0];
                int j1 = j + fMoore[a, 1];
                if (i1 >= xSize || i1 < 0 || j1 >= ySize || j1 < 0) continue;
                if (tabTemp[i1, j1] == null || tabTemp[i1, j1] == -1) continue;
                if (dictionary.ContainsKey(tabTemp[i1, j1]))
                    dictionary[tabTemp[i1, j1]]++;
                else
                    dictionary.Add(tabTemp[i1, j1], 1);
            }

            int maxId = FindMaxId(dictionary);
            if (maxId == 0 || dictionary[maxId] < 3)
                return false;

            tab[i, j] = new Seed(maxId);
            AddPoint(i, j, bit, colors[maxId - 1]);
            return true;
        }

        #endregion

        #region rekrystalizacja

        private void DistributeEnergy(int grainE, Seed[,] tab) {
            DistributeEnergy(false, grainE, -1, tab);
        }

        private void DistributeEnergy(bool heterogenous, int grainE, int borderE, Seed[,] tab) {

            CheckOnBorder(tab);

            Parallel.For(0, MCSizeX, (index) => {
                for (int j = 0; j < MCSizeY; j++) {
                    if (tab[index, j].OnBorder && heterogenous) {
                        tab[index, j].H = borderE;
                    }
                    else {
                        tab[index, j].H = grainE;
                    }
                }
            });
        }

        private void CheckOnBorder(Seed[,] tab) {
            for (int i = 0; i < MCSizeX; i++) {
                for (int j = 0; j < MCSizeY; j++) {
                    if (tab[i, j].OnBorder) continue;
                    for (int a = -1; a <= 1; a++) {
                        for (int b = -1; b <= 1; b++) {
                            if (a == i && b == j) continue;
                            if (!FitsBoundaries(i + a, j + b)) continue;
                            // if (a < 0 || a >= MCSizeX || b < 0 || b >= MCSizeY) continue;
                            if (tab[i + a, j + b].Id != tab[i, j].Id) {
                                tab[i + a, j + b].OnBorder = true;
                                tab[i, j].OnBorder = true;
                                a = 2;
                                b = 2;
                            }
                        }
                    }
                }
            }
        }

        private void button9_Click(object sender, EventArgs e) {
            SRXMC();
        }

        private void InitialStructureMCCombo_SelectedIndexChanged(object sender, EventArgs e) {
            label24.Visible = label25.Visible = InitStructureMCField1.Visible =
                InitStructureMCField2.Visible = true;
            if ((sender as ComboBox).SelectedIndex == 0) {
                label24.Text = "Nucleon amount";
                label25.Text = "Probability";
            }
            else {
                label24.Text = "State number";
                label25.Text = "Simulation steps";
            }
        }

        private void StoredEnergyMCCombo_SelectedIndexChanged(object sender, EventArgs e) {
            var b = (sender as ComboBox).SelectedIndex == 1;

            label19.Visible = BoundaryEnergyMCField.Visible = b;
        }

        private void SRXMC() {

            if (NucleonMCCombo.SelectedIndex == -1 || NucleonLocationMCCombo.SelectedIndex == -1 || StoredEnergyMCCombo.SelectedIndex == -1)
                return; // nie wypełniono danych

            // zebranie danych 1
            // odpalenie MC albo CA

            if (InitialStructureMCCombo.SelectedIndex == 0) { // CA
                N = (int)InitStructureMCField1.Value;
                MCDualPhaseProb = (int)InitStructureMCField2.Value;
                MCSizeX = PictureBoxMonteCarlo.Width;
                MCSizeY = PictureBoxMonteCarlo.Height;

                TabMC = new Seed[MCSizeX, MCSizeY];
                Checked = new bool[MCSizeX, MCSizeY];
                MCColors = new List<Color>();
                CheckedAmount = 0;
                Bitmap bit = new Bitmap(MCSizeX, MCSizeY);
                Random r = new Random();
                InsertNucleons(N, TabMC, MCColors, bit, r);
                Simulate(TabMC, bit, N);

                for(int i = 0; i < 400; i++) {
                    for(int j = 0; j < 400; j++) {
                        if(TabMC[i, j] == null) {
                            Console.WriteLine(i + ", " + j);
                        }
                    }
                }
            }
            else if(InitialStructureMCCombo.SelectedIndex == 1) { // MC
                N = (int)InitStructureMCField1.Value;
                MCSteps = (int)InitStructureMCField2.Value;
                BoundaryEnergy = 1.0;
                MCSizeX = PictureBoxMonteCarlo.Width;
                MCSizeY = PictureBoxMonteCarlo.Height;

                TabMC = new Seed[MCSizeX, MCSizeY];
                Checked = new bool[MCSizeX, MCSizeY];
                MCColors = new List<Color>();
                CheckedAmount = 0;
                MonteCarloSimulation();
            }
            else {
                return; // nie wypełniono danych
            }

            // DistributeEnergy()

            DistributeEnergy(StoredEnergyMCCombo.SelectedIndex == 1, (int)EnergyMCField.Value,
                (int)BoundaryEnergyMCField.Value, TabMC);

            
            Recrystalization(TabMC, (int)NucleonMCField.Value, NucleonMCCombo.SelectedIndex,
                NucleonLocationMCCombo.SelectedIndex, (int)SimulationStepRecField.Value);
            
            // zebranie danych 2

            // rekrystalizacja
        }

        private void Recrystalization(Seed[,] tab, int nAmount, int nDistribution, int nLoc, int steps) {
            Bitmap Bitm = PictureBoxMonteCarlo.Image as Bitmap;
            Dictionary<int, int> data;

            Random RandomGen = new Random();
            data = new Dictionary<int, int>();

            int nucleonsToAdd = 0;
            if (nDistribution == 1) {
                nucleonsToAdd = nAmount;
            }
            List<Color> recColor = new List<Color>();

            InsertNucleons(tab, Bitm, recColor, nAmount, RandomGen, nLoc);
            bool[,] Checked = new bool[MCSizeX, MCSizeY];

            int[,] directions = new int[8, 2] 
                { { 1, 0 }, { 1, 1 }, { 0, 1 }, { -1, 1 }, { -1, 0 }, { -1, -1 }, { 0, -1 }, { 1, -1 } };

            for (int i = 0; i < steps; i++) {
                int x = -1;
                int y = -1;

                if (nDistribution == 1) {
                    nAmount += nucleonsToAdd;
                }

                if (nDistribution == 0 || nDistribution == 1) {
                    // new nucleons
                    InsertNucleons(tab, Bitm, recColor, nAmount, RandomGen, nLoc);
                }


                while (CheckedAmount < MCSizeY * MCSizeX) {
                    do {
                        x = RandomGen.Next(MCSizeX);
                        y = RandomGen.Next(MCSizeY);
                    }
                    while (Checked[x, y] || tab[x, y] == -1);
                    CheckedAmount++;

                    int d = -1;
                    do {
                        d = RandomGen.Next(0, directions.GetLength(0));
                    }
                    while (!FitsBoundaries(x + directions[d, 0], y + directions[d, 1]));

                    int xs = x + directions[d, 0];
                    int ys = y + directions[d, 1];

                    if (TabMC[xs, ys] < 0) {
                        int eBef = GetEnergy(tab, x, y, directions);
                        int eAft = GetEnergy(tab, x, y, directions, TabMC[xs, ys].Id);
                        if(eBef > eAft) {
                            tab[x, y].Id = tab[xs, ys].Id;
                            tab[x, y].H = 0;
                            Bitm.SetPixel(x, y, recColor[-tab[x, y].Id - 1]);
                        }
                    }

                    
                    // Checked[x, y] = true;
                    // CheckPixel(x, y, RandomGen, data, Bitm);
                }

                for (int k = 0; k < 400; k++)
                    for (int j = 0; j < 400; j++) {
                        Checked[k, j] = false;
                    }
                CheckedAmount = 0;
                PictureBoxMonteCarlo.Refresh();
            }
        }

        private bool FitsBoundaries(int x, int y) {
            return x < MCSizeX && x >= 0 && y < MCSizeY && y >= 0;
        }

        private int GetEnergy(Seed[,] tab, int i, int j, int[,] dirs, int id = 0) {
            int result = 0;
            for (int a= 0; a < dirs.GetLength(0); a++) {
                int x = i + dirs[a, 0];
                int y = j + dirs[a, 1];
                if (!FitsBoundaries(x, y))
                    continue;
                if (id == 0) {
                    if (tab[x, y].Id != tab[i, j].Id)
                        result++;
                }
                else if(tab[x, y].Id != id) {
                    result++;
                }
            }
            if (id==0) {
                result += tab[i, j].H;
            }

            return result;
        }

        private void InsertNucleons(Seed[,] tab, Bitmap bit, List<Color> colors, int nucAm, Random rand, int place) {
            for (int i = 1; i <= nucAm; i++) {
                int a = 0;
                int b = 0;

                do {
                    a = rand.Next(MCSizeX);
                    b = rand.Next(MCSizeY);
                }
                while (place != 1 && !tab[a,b].OnBorder);
                
                tab[a, b].Id = -i;
                if(colors.Count <= i - 1) {
                    colors.Add(Color.FromArgb(rand.Next(50, 256), 0, 0));
                }
                AddPoint(a, b, bit, colors[i - 1]);
            }
        }


        #endregion

    }

    //public enum Structure {
    //    None = 0,
    //    Substructure = 1,
    //    DualPhase = 2

    //}

    public class ImportTxtHelper {
        [JsonProperty("xSize")]
        public int xSize { get; set; }

        [JsonProperty("ySize")]
        public int ySize { get; set; }

        [JsonProperty("nucAmount")]
        public int NucAmount { get; set; }

        [JsonProperty("inclusionAmount")]
        public int InclusionAmount { get; set; }

        [JsonProperty("inclusionRadius")]
        public int InclusionRadius { get; set; }

        [JsonProperty("inclusionType")]
        public string InclusionType { get; set; }

        [JsonProperty("inclusionsCenter")]
        public Point[] InclusionsCenter { get; set; }
        
        [JsonProperty("tab")]
        public Seed[,] Tab { get; set; }
    }
}
